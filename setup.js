const logger = require('winston');

var config = require('./config.json');
var Model = require('./app/model');

var model = Model(config, undefined, function () {
    var username = 'admin';
    var password = 'changeme';

    var admin = new model.AdminUser({
        name: 'Administrator',
        lastName: '(Default)',
        email: `admin@${config.siteDomain}`,
        username: username,
        password: password
    });
    admin.hashPassword().then(() => {
        admin.save(function (err) {
            if (err) {
                logger.error('[Setup]', 'Failed to set up administrator user');
                logger.error('[Setup]', err);
                process.exit(-1);
            }

            logger.info('[Setup]', 'Set up a default administrator user');
            logger.info('[Setup]', 'Username:', username);
            logger.info('[Setup]', 'Password:', password);
            process.exit(0);
        });
    });
});
