// This file is used to define custom error types required by other modules

function LoginError(message) {
    this.name = 'LoginError';
    this.message = (message || '');
    this.stack = new Error().stack;
}
LoginError.prototype = Object.create(Error.prototype);

function RegisterError(message) {
    this.name = 'RegisterError';
    this.message = (message || '');
    this.stack = new Error().stack;
}
RegisterError.prototype = Object.create(Error.prototype);

function ValidationError(message) {
    this.name = 'ValidationError';
    this.message = (message || '');
    this.stack = new Error().stack;
}
ValidationError.prototype = Object.create(Error.prototype);

module.exports = {
    LoginError: LoginError,
    RegisterError: RegisterError,
    ValidationError: ValidationError
};