'use strict';

/**
 * Module depedencies
 */
const Promise = require('bluebird');
const bcrypt = require('bcryptjs');
const uuid = require('node-uuid');

const LoginError = require('./errors').LoginError;
const RegisterError = require('./errors').RegisterError;
const ValidationError = require('./errors').ValidationError;

var AbstractUserModel;
var AdminUserModel;
var LocalUserModel;
var GoogleUserModel;

function _hashPassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(password, salt, function(err, hash) {
                if (err) reject(err);
                else resolve(hash);
            });
        });
    });
}

/**
 * Static methods
 */

function exists(user) {
    return AbstractUserModel.find(user).count().lean().exec();
}

function findAllUsers() {
    return AbstractUserModel.find().exec();
}

function findByUid(uid) {
    return AbstractUserModel
    .find({ _id: uid })
    .select('-_id type name lastName username email sex')
    .exec()
    .then(res => res[0]);
}

function findByUsername(username) {
    return AbstractUserModel.find({ username: username }).exec().then(res => res[0]);
}

function findOrCreate(user) {
    return AbstractUserModel.find({ _id: user._id }).exec().then(res => {
        if (!res || !res.length) {
            return AbstractUserModel.create(user).exec();
        }
        return res;
    });
}

function findOrCreateGoogle(user) {
    return GoogleUserModel.find({ googleID: user.googleID }).exec().then(res => {
        if (!res || !res.length) {
            return new GoogleUserModel(user).save();
        }
        return res[0];
    });
}

/* 
 * Used to validate if a user password combination exists.
 * The user argument has a password property containing an unhashed password.
 * Returns the User document in storage if resolved.
 * @param {Object} user
 */
function login(user) {
    return new Promise((resolve, reject) => {
        AbstractUserModel.findOne({ username: user.username }).lean().exec((err, dbUser) => {
            if (err) reject(err);
            else if (!dbUser) reject(new LoginError('Incorrect username'));
            else bcrypt.compare(user.password, dbUser.password, function(err, res) {
                if (err) reject(err);
                // TODO: If this message passes through it should be less specific.
                else if (!res) reject(new LoginError('Incorrect password'));
                else resolve(dbUser);
            });
        });
    });
}

function signUpLocal(user) {
    var newUser = new LocalUserModel(user);    
    return newUser.checkValid().catch(err => {
        throw new RegisterError(err);
    }).then((newUser) => {
        return newUser.hashPassword();
    }).then((newUser) => {
        return newUser.save();
    });
}

/**
 * Instance methods
 */

function checkInstancePassword(password) {
    return new Promise((resolve, reject) => {
        bcrypt.compare(password, this.password, (err, res) => {
            if (err) reject(err);
            resolve(res);
        });
    });
}

function hashInstancePassword() {
    return _hashPassword(this.password).then((hash) => {
        this.password = hash;
        return this;
    });
}

function validateInstance() {
    return new Promise((resolve, reject) => {
        this.validate(err => {
            if (err) reject(new ValidationError(err));
            else resolve(this);
        });
    });
}

// Constructor, mongoose injected
function AbstractUser(mongoose) {
    const Schema = mongoose.Schema;
    const options = { collection: 'users', discriminatorKey: 'type' };
    const childOptions = { _id: false, collection: 'users', discriminatorKey: 'type' };
    
    const abstractUserSchema = new Schema({
        _id: {
            type: String,
            default: uuid.v4
        },
        name: {
            type: String,
            required: true
        },
        lastName: {
            type: String,
            required: true
        }
    }, options);

    abstractUserSchema.statics = {
        exists: exists,
        findAllUsers: findAllUsers,
        findByUid: findByUid,
        findOrCreate: findOrCreate,
        login: login,
    };

    abstractUserSchema.methods = {
    };

    const adminUserSchema = new Schema({
        email: {
            type: String,
            match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'This is not a valid email.'],
            required: true
        },
        username: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        }
    }, childOptions);

    adminUserSchema.statics = {
        findByUsername: findByUsername
    };

    adminUserSchema.methods = {
        hashPassword: hashInstancePassword,
        checkPassword: checkInstancePassword,
        checkValid: validateInstance
    };

    const localUserSchema = new Schema({
        email: {
            type: String,
            match: [/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/, 'This is not a valid email.'],
            required: true
        },
        username: {
            type: String,
            required: true,
            unique: true
        },
        password: {
            type: String,
            required: true
        }
    }, childOptions);

    localUserSchema.statics = {
        findByUsername: findByUsername,
        signUp: signUpLocal
    };

    localUserSchema.methods = {
        hashPassword: hashInstancePassword,
        checkPassword: checkInstancePassword,
        checkValid: validateInstance
    };

    const googleUserSchema = new Schema({
        googleID: {
            type: String,
            required: true,
            unique: true
        },
        emails: {
            type: [String],
        }
    }, childOptions);

    googleUserSchema.statics = {
        findOrCreate: findOrCreateGoogle
    };

    AbstractUserModel = mongoose.model('AbstractUser', abstractUserSchema);
    AdminUserModel = AbstractUserModel.discriminator('AdminUser', adminUserSchema);
    LocalUserModel = AbstractUserModel.discriminator('LocalUser', localUserSchema);
    GoogleUserModel = AbstractUserModel.discriminator('GoogleUser', googleUserSchema);

    return {
        AbstractUser: AbstractUserModel,
        AdminUser: AdminUserModel,
        LocalUser: LocalUserModel,
        GoogleUser: GoogleUserModel
    };
}

module.exports = AbstractUser;