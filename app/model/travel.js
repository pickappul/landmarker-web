'use strict';

/**
 * Module dependencies
 */
const uuid = require('node-uuid');
const Promise = require('bluebird');

var TravelModel;

/**
 * Static methods
 */

/*
 * Function that recieves a travel object and saves it to mongodb. If an array of travels is provided it saves all of them.
 */
function store(travel){
    if (travel.length === undefined)
    {    
        return TravelModel(travel).save();
    }else
    {
        var travelSaves = [];
        for (var i = travel.length - 1; i >= 0; i--) {
            travelSaves.push(TravelModel(travel[i]).save());
        }
        return Promise.all(travelSaves);
    }
}

function findOne(id){
    return TravelModel
    .find({
        _id: id
    })
    .lean()
    .exec();
}

function findAll(){
    return TravelModel
    .find()
    .select({
        _id: 1,
        name: 1,
        iniLoc: 1
    })
    .lean()
    .exec();
}

function findOneCheckpoints(id) {
    return TravelModel.find({
        _id: id
    }, 'points').exec().then(res => res[0].points.map(item => ({
        name: item.name,
        coordinates: item.loc.coordinates,
        question: item.question,
        answer: item.answer,
        options: item.options
    })));
}

/*
 * This function returns a query promise. The query after executed must return a list of travels ordered from closest to farthest. 
 * @param {Number} maxDistance: The max distance in meters from the center to search for travels
 * @param {Number} longitude: The angular distance, in decimal format, of a point east or west of the Prime (Greenwich) Meridian
 * @param {Number} latitude: The angle in decimal format which ranges from 0° at the Equator to 90° (North or South) at the poles
 */
function findNear(maxDistance, longitude, latitude){
    var geoNear = {
        near: { type: 'Point', coordinates: [longitude, latitude] },
        distanceField: 'distance',
        maxDistance: maxDistance,
        spherical: true
    };

    var projection = {
        _id: 0,
        id: '$_id',
        name: 1,
        coordinates: '$iniLoc.coordinates',
        distance: 1,
        image: 1,
        description: 1
    };

    return TravelModel
    .aggregate([
    { $geoNear: geoNear },
    { $project: projection}
    ])
    .exec();
}

function updateOne(id, travel){
    return TravelModel.update({
        _id: id
    }, {
        $set: travel
    })
    .exec();
}

function removeOne(id){
    return TravelModel.remove({
        _id: id
    })
    .exec();
}

/**
 * Instance methods
 */

/**
 * Constructor
 * Index 2dsphere is required on iniLoc
 * Sample index creation: db.travels.createIndex( { "iniLoc" : "2dsphere" } );
 */
function Travel(mongoose){
    const Schema = mongoose.Schema;

    /*
     * GeoJsonPoint schema
     * The numbers for coordinates must be [<longitude> , <latitude>]
     */
    const geoJsonPoint = new Schema({
        _id: false,
        type: {
            type: String,
            default: "Point"
        },
        coordinates: {
            type: [Number],
            required: true
        }
    });

    /**
     * Point schema
     */
    const pointSchema = new Schema({
        _id: false,
        name: {
            type: String,
            required: true
        },
        loc: {
            type: geoJsonPoint,
            required: true
        },
        question: {
            type: String,
            required: true
        },
        answer: {
            type: String,
            required: true
        },
        options: {
            type: [String],
            required: true
        }
    });

    /**
     * Travel schema
     */
    const travelSchema = new Schema({
        _id: {
            type: String,
            default: uuid.v4
        },
        name: {
            type: String,
            required: true
        },
        iniLoc: {
            type: geoJsonPoint,
            required: true
        },
        image: {
            type: String
        },
        description: {
            type: String
        },
        points: {
            type: [pointSchema],
            required: true
        }
    });

    travelSchema.statics = {
        store: store,
        findNear: findNear,
        updateOne: updateOne,
        findOne: findOne,
        findOneCheckpoints: findOneCheckpoints,
        removeOne: removeOne,
        findAll: findAll
    };

    travelSchema.methods = {

    };

    TravelModel = mongoose.model('Travel', travelSchema);
    return TravelModel;
}

module.exports = Travel;