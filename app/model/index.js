const Promise = require('bluebird');
const logger = require('winston');

// Constructor, allows overriding mongoose
function Model(config, mongoose, callback) {
    if (!mongoose) mongoose = require('mongoose');
    logger.info('[DB]', 'Initializing a new MongoDB connection to', config.db.uri);
    mongoose.connect(config.db.uri);

    mongoose.connection.on('connected', () => {
        logger.info('[DB]', 'Connection initialized', config.db.uri);

        mongoose.Promise = Promise; // Use Bluebird Promises

        if (callback !== undefined) callback();
    });

    // Load schemas
    let UserModule = require('./user')(mongoose);

    const model = Object.freeze({
        AbstractUser: UserModule.AbstractUser,
        AdminUser: UserModule.AdminUser,
        LocalUser: UserModule.LocalUser,
        GoogleUser: UserModule.GoogleUser,
        Travel: require('./travel')(mongoose),
        DoneTravel: require('./doneTravel')(mongoose)
    });
    return model;
}

module.exports = Model;