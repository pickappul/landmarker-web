'use strict';

/**
 * Module dependencies
 */
const uuid = require('node-uuid');

var DoneTravelModel;

/**
 * Static methods
 */

function store(doneTravel){
	return DoneTravelModel(doneTravel).save();
}

function finishTravel(doneTravelId, distanceTraveled){
	var finishDate = Date.now();
	var speed;

	return DoneTravelModel.findOne({
		_id: doneTravelId
	})
	.exec()
	.then((doneTravel) => {
		speed = distanceTraveled*1000/(finishDate-doneTravel.startDate.getTime());
		doneTravel.speed = speed;
		doneTravel.distanceTraveled = distanceTraveled;
		doneTravel.finishDate = finishDate;
		return doneTravel.save();
	});
}

function findAllForUser(userId){
	return DoneTravelModel
	.find({
		_user: userId
	})
	.populate({
		path: '_travel',
		select: '-_id name iniLoc description'
	})
	.select({
		_user: 0,
		__v: 0
	})
	.sort({
		startDate: -1
	})
	.lean()
	.exec();
}

/**
 * Instance methods
 */

/**
 * Constructor
 */
function DoneTravel(mongoose){
	const Schema = mongoose.Schema;

	const doneTravelSchema = new Schema({
		_id: {
			type: String,
			default: uuid.v4
		},
		_user: {
			type: String,
			ref: 'AbstractUser',
			required: true
		},
		_travel: {
			type: String,
			ref: 'Travel',
			required: true
		},
		startDate: {
			type: Date,
			default: Date.now
		},
		finishDate: {
			type: Date
		},
		distanceTraveled: {
			type: Number
		},
		speed: {
			type: Number
		}
	});

	doneTravelSchema.statics = {
		store: store,
		finishTravel: finishTravel,
		findAllForUser: findAllForUser
	};

	doneTravelSchema.methods = {};

	DoneTravelModel = mongoose.model('DoneTravel', doneTravelSchema);
	return DoneTravelModel;
}

module.exports = DoneTravel;