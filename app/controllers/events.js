'use strict';

var visitors = {};
var socket;
var io;

var events = {
    chatMessage: function(msg){
        io.emit('chat message', msg);
        console.log('Mensaje: ' + msg);
    },
    newUser: function(data){
        if(parseInt(Object.keys(visitors).length) > 0)
            socket.emit('already', {visitors: visitors});
        visitors[socket.id] = data.pos;
        io.emit('connected', { pos: data.pos, users_count: Object.keys(visitors).length });
        console.log('Alguien se conectó:');
        console.log(visitors);
    },
    disconnect: function(){
        if(visitors[socket.id]){
            var todel = visitors[socket.id];
            delete visitors[socket.id];
            io.emit('disconnected', { del: todel, users_count: Object.keys(visitors).length });
        }
        console.log('Alguien se desconectó:');
        console.log(visitors);
    }
};

module.exports = function(mainSocket, mainIo){
    socket = mainSocket;
    io = mainIo;
    return events;
};