'use strict';

const logger = require('winston');

var _model;

function store(req, res){
    _model.DoneTravel.store(req.body)
    .then((doneTravel) => {
        res.json({msg: doneTravel.id});
    })
    .catch(err => {
        logger.error('[DoneTravelsController]', err);
        res.json({msg: err});
    });
}

function finishTravel(req, res){
    _model.DoneTravel.finishTravel(req.params.id, req.body.distanceTraveled)
    .then(() => {
        res.json({msg: 'ok'});
    })
    .catch(err => {
        logger.error('[DoneTravelsController]', err);
        res.json({msg: err});
    });
}

function findAllForUser(req, res){
    _model.DoneTravel.findAllForUser(req.params.id)
    .then((doneTravels) => {
        res.send(doneTravels.map(doneTravel => ({
            _id: doneTravel._id,
            _user: doneTravel._user,
            _travel: doneTravel._travel,
            startDate: doneTravel.startDate.getTime(),
            finishDate: doneTravel.finishDate.getTime(),
            distanceTraveled: doneTravel.distanceTraveled,
            speed: doneTravel.speed
        })));
    })
    .catch(err => {
        logger.error('[DoneTravelsController]', err);
        res.json({msg: err});
    });
}

function DoneTravelController(model){
    _model = model;

    var doneTravelController = Object.freeze({
        store: store,
        finishTravel: finishTravel,
        findAllForUser: findAllForUser
    });

    return doneTravelController;
}

module.exports = DoneTravelController;