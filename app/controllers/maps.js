'use strict';

var path = require('path');

function index(req, res){
	res.render('index', {loggedIn: req.isAuthenticated()});
}

function infoPage(req, res){
	res.render('info-page');
}


module.exports = {
	index: index,
	infopage: infoPage
}
