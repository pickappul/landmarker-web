function Controllers(model, config) {
    var controllers = {
        events: require('./events'),
        maps: require('./maps'),
        users: require('./users')(model),
        auth: require('./auth')(model, config),
        travels: require('./travels')(model),
        doneTravels: require('./doneTravels')(model)
    };
    return controllers;
}

module.exports = Controllers;