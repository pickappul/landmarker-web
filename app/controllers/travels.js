'use strict';

/**
 * Module depedencies
 */
const Promise = require('bluebird');
const logger = require('winston');

var _model;

function _formToTravel(formTravel){
    return new Promise((resolve, reject) => {
        if (isNaN(formTravel.longitude) || isNaN(formTravel.latitude)) {
            reject('La latitud o la longitud no es válida.');
        }

        var travel = {
            name: formTravel.name,
            iniLoc: {
                type: 'Point',
                coordinates: [formTravel.longitude, formTravel.latitude]
            },
            image: formTravel.image,
            description: formTravel.description,
            points: []
        };

        var i = 1;
        var checkpointName = formTravel['checkpoint-name-'.concat(i)];
        while(checkpointName !== undefined){
            var checkpointQuestion = formTravel['checkpoint-question-'.concat(i)];
            var checkpointAnswer = formTravel['checkpoint-answer-'.concat(i)];
            var checkpointLatitude = formTravel['checkpoint-latitude-'.concat(i)];
            var checkpointLongitude = formTravel['checkpoint-longitude-'.concat(i)];

            if (isNaN(checkpointLatitude) || isNaN(checkpointLongitude)) {
                reject('Latitude or longitude is invalid');
            }

            var point = {
                name: checkpointName,
                loc: {
                    type: 'Point',
                    coordinates: [checkpointLongitude, checkpointLatitude]
                },
                question: checkpointQuestion,
                answer: checkpointAnswer,
                options: []
            };

            var j = 1;
            var checkpointOption = formTravel['checkpoint-option-'.concat(i, '-', j)];
            while(checkpointOption !== undefined){
                point.options.push(checkpointOption);
                j++;
                checkpointOption = formTravel['checkpoint-option-'.concat(i, '-', j)];
            }
            travel.points.push(point);
            i++;
            checkpointName = formTravel['checkpoint-name-'.concat(i)];
        }
        resolve(travel);
    });
}

function store(req, res){
    var operation;

    if (req.is('application/json')) {
        operation = _model.Travel.store(req.body);
    }
    else if (req.is('application/x-www-form-urlencoded')){
        operation = _formToTravel(req.body)
        .then((travel) => {
            _model.Travel.store(travel);
        });
    }

    operation
    .then(() => {
        res.send({msg: 'ok'});
    })
    .catch(err => {
        res.json({msg: err});
    });
}

function createOne(req, res){
    res.render('travel-new', {});
}

function serveFindOne(req, res){
    _model.Travel.findOne(req.params.id)
    .then(travels => {
        res.render('travel-edit', {travel: travels[0]});
    })
    .catch(err => {
        logger.error('[TravelsController]', err);
        res.json({msg: err});
    });
}

function findOne(req, res){
    _model.Travel.findOne(req.params.id)
    .then(travels => {
        res.json({travel: travels[0]});
    })
    .catch(err => {
        logger.error('[TravelsController]', err);
        res.json({msg: err});
    });
}

function findOneCheckpoints(req, res) {
    _model.Travel.findOneCheckpoints(req.params.id)
    .then(points => {
        res.json(points);
    })
    .catch(err => {
        logger.error('[TravelsController]', err);
        res.json({msg: err});
    });
}

function findAll(req, res){
    _model.Travel.findAll()
    .then(travels => {
        res.render('travels-table', {travels: travels});
    })
    .catch(err => {
        logger.error('[TravelsController]', err);
        res.json({msg: err});
    });
}

function findNear(req, res){
    var maxDistance = req.params.maxDistance;
    var longitude = req.params.longitude;
    var latitude = req.params.latitude;

    _model.Travel.findNear(parseFloat(maxDistance), parseFloat(longitude), parseFloat(latitude))
    .then((travels) => {
        res.json(travels);
    })
    .catch((err) => {
        res.json({msg: err});
    });
}

function updateOne(req, res){
    var id = req.params.id;
    var formTravel = req.body;

    _formToTravel(formTravel)
    .then((travel) => {
        _model.Travel.updateOne(id, travel);
    })
    .then((raw) => {
        res.json({msg: raw});
    })
    .catch((err) => {
        res.status(500).send(err);
    });
}

function removeOne(req, res){
    _model.Travel.removeOne(req.params.id)
    .then(() => {
        res.json({msg: 'ok'});
    })
    .catch((err) => {
        res.json({msg: err});
    });
}

function TravelsController(model){
    _model = model;

    var travelController = Object.freeze({
        store: store,
        createOne: createOne,
        findNear: findNear,
        updateOne: updateOne,
        findOne: findOne,
        serveFindOne: serveFindOne,
        findOneCheckpoints: findOneCheckpoints,
        removeOne: removeOne,
        findAll: findAll
    });

    return travelController;
}

module.exports = TravelsController;
