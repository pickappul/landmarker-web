'use strict';

const logger = require('winston');

var _model;

function _transformSignUpErrors(err){
    var userFieldErrors = [];
    if (err.errors !== undefined) {
        for (var property in err.errors){
            if (err.errors.hasOwnProperty(property)) {
                userFieldErrors.push(property);
            }
        }
    }
    if(err.code !== undefined && err.errmsg !== undefined){
        userFieldErrors.push('username');
    }
    return userFieldErrors;
}

function signUp(req, res){
    _model.LocalUser.signUp(req.body)
    .then(() => {
        res.json({msg: 'ok'});
    })
    .catch(err => {
        logger.error('[UsersController]', err);
        res.send({errors: _transformSignUpErrors(err)});
    });
}

function findAllUsers(req, res){
    return _model.AbstractUser.findAllUsers()
        .then(users => {
            return res.render('users-list', { users: users });
        });
}

function findByUid(req, res){
    _model.AbstractUser.findByUid(req.params.id)
    .then(user => {
        res.json(user);
    })
    .catch(err => {
        logger.error('[UsersController]', err);
        res.send({msg: err});
    });
}

function UsersController(model){
    _model = model;

    return Object.freeze({
        signUp: signUp,
        findAllUsers: findAllUsers,
        findByUid: findByUid
    });
}

module.exports = UsersController;