'use strict';

/**
 * Module dependencies
 */
const logger = require('winston');
const PassportLocalStrategy = require('passport-local').Strategy;
const PassportGoogleStrategy = require('passport-google-oauth20').Strategy;
const LoginError = require('../model/errors').LoginError;

var _model;

function LocalStrategy(config) {
    return new PassportLocalStrategy(function (username, password, done) {
        _model.AbstractUser.login({ username: username, password: password }).then(user => {
            done(null, user);
        }).catch(LoginError, () => {
            done(null, false);
        });
    });
}

function GoogleStrategy(config) {
    return new PassportGoogleStrategy({
        clientID: config.auth.google.clientID,
        clientSecret: config.auth.google.clientSecret,
        callbackURL: `${config.siteProtocol}://${config.siteDomain}/login/google/callback`
    }, function (accessToken, refreshToken, profile, done) {
        _model.GoogleUser.findOrCreate({
            googleID: profile.id,
            name: profile.name.givenName,
            lastName: profile.name.familyName,
            emails: profile.emails.map(email => email.value)
        }).then(user => {
            done(null, user);
        }).catch(err => {
            logger.error(err);
            done(err);
        });
    });
}

function serializeUser(user, done) {
    done(null, user._id);
}

function deserializeUser(uid, done) {
    _model.AbstractUser.findByUid(uid).then((user) => {
        done(null, user);
    }, (err) => {
        done(err);
    });
}

function adminLogin(req, res) {
    res.render('login');
}

function adminRequired(req, res, next) {
    if (req.user && req.user.type === 'AdminUser') next();
    else res.sendStatus(401);
}

function AuthController(model, config) {
    _model = model;
    
    var authController = Object.freeze({
        strategies: [],
        serializeUser: serializeUser,
        deserializeUser: deserializeUser,
        adminLogin: adminLogin,
        adminRequired: adminRequired
    });

    if (!config.auth) {
        logger.error('[Auth]', 'No authentication method set up.');
        process.exit();
    }

    if (config.auth.local) {
        authController.strategies.push(LocalStrategy(config));
        logger.info('[Auth]', 'Local authentication set up.');
    }

    if (config.auth.google) {
        authController.strategies.push(GoogleStrategy(config));
        logger.info('[Auth]', 'Google authentication set up.');
    }

    return authController;
}

module.exports = AuthController;