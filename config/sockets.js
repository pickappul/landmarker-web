'use strict';

function setupSockets(io, controllers){
	
	var assignEvents = function(socket){
		console.log('connected');
		var events = controllers.events(socket, io);
		socket.on('chat message', events.chatMessage);
		socket.on('new_user', events.newUser);
		socket.on('disconnect', events.disconnect);
	}
	io.on('connection', assignEvents);
}

module.exports = setupSockets;