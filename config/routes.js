'use strict';

var passport = require('passport');

function setupRoutes(app, config, controllers){
    app.get('/', controllers.maps.index);
    app.get('/info-page', controllers.maps.infopage);
    app.post('/register', controllers.users.signUp);

    // Authentication
    if (config.auth.local) {
        app.post('/login', function (req, res, next) {
            passport.authenticate('local', function (err, user, info) {
                if (err) return next(err);
                if (!user) {
                    if (req.accepts('text/html')) return res.redirect('/login');
                    if (req.accepts('application/json')) return res.status(401).json({ status: 'bad' });
                    return res.redirect('/login');
                }
                req.login(user, function (err) {
                    if (err) return next(err);
                    if (req.accepts('text/html')) return res.redirect('/');
                    if (req.accepts('application/json')) return res.status(200).json({ status: 'ok', uid: user._id });
                    return res.redirect('/');
                });
            })(req, res, next);
        });
    }

    if (config.auth.google) {
        app.get('/login/google',
            passport.authenticate('google', { scope: [
                'https://www.googleapis.com/auth/plus.login',
                'https://www.googleapis.com/auth/plus.profile.emails.read'
            ]}));
        app.get('/login/google/callback',
            passport.authenticate('google', { failureRedirect: '/login' }),
            function(req, res) {
                res.redirect('/');
            });
    }

    /*
     * Travel routes
     */
    app.get('/travel/near/:maxDistance/:latitude/:longitude', controllers.travels.findNear);
    app.get('/travel/:id', controllers.travels.findOne);
    app.get('/travel/:id/points', controllers.travels.findOneCheckpoints);
    app.get('/user/:id', controllers.users.findByUid);
    
    //For admin

    /*
     * Achievement routes
     */
    app.post('/done_travel', controllers.doneTravels.store);
    app.post('/done_travel/:id', controllers.doneTravels.finishTravel);
    app.get('/done_travel/user/:id', controllers.doneTravels.findAllForUser);

    /*
     * For admin
     */
    app.get('/admin', controllers.auth.adminLogin);
    app.get('/admin/travel/:id', controllers.auth.adminRequired, controllers.travels.serveFindOne);
    app.get('/admin/travels', controllers.auth.adminRequired, controllers.travels.findAll);
    app.get('/admin/users', controllers.auth.adminRequired, controllers.users.findAllUsers);
    app.get('/admin/travel', controllers.travels.createOne);
    app.post('/admin/travel', controllers.travels.store);
    app.put('/admin/travel/:id', controllers.travels.updateOne);
    app.delete('/admin/travel/:id', controllers.travels.removeOne);

}

module.exports = setupRoutes;
