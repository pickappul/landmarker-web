'use strict';

const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const RedisStore = require('connect-redis')(session);
const logger = require('winston');

const nunjucks = require('nunjucks');
const socketIo = require('socket.io');
const ioRedis = require('socket.io-redis');
const passport = require('passport');

var config = require('./config.json');
var Model = require('./app/model');
var Controllers = require('./app/controllers');
var routes = require('./config/routes');
var sockets = require('./config/sockets');

var model = Model(config);
var controllers = Controllers(model, config);

// Setup express and socketIO
var app = express();
var server = http.Server(app);
var io = socketIo(server);

// Use redis for socket.IO
io.adapter(ioRedis(config.redis));

// Configure template engine
nunjucks.configure('views', {
    autoescape: true,
    express: app
});

// Set template engine for express
app.set('view engine', 'njk');

// Static content
app.use(express.static(__dirname + '/public'));

// Middleware
app.use(cookieParser(config.session.secret));
app.use(bodyParser.urlencoded({ 
    extended: true 
}));
app.use(bodyParser.json());
app.use(session({
    store: new RedisStore(config.redis),
    secret: config.session.secret,
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false, maxAge: 86400000 }
}));

// Authentication
passport.serializeUser(controllers.auth.serializeUser);
passport.deserializeUser(controllers.auth.deserializeUser);
controllers.auth.strategies.forEach(strategy => passport.use(strategy));
app.use(passport.initialize());
app.use(passport.session());

// Setup routes
routes(app, config, controllers);

// Setup sockets
sockets(io, controllers);

// Listen (default 3000)
server.listen(process.env.PORT || config.port || 3000, function(){
    logger.info('[App]', `Listening at port ${process.env.PORT || config.port || 3000}!`);
});
