'use strict';

var Mongoose = require('mongoose').Mongoose;
var mongoose = new Mongoose();

const Promise = require('bluebird');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonTest = require('sinon-test');
const mockgoose = require('mockgoose');
const logger = require('winston');

chai.use(chaiAsPromised);
const assert = chai.assert;
sinon.test = sinonTest.configureTest(sinon);
sinon.testCase = sinonTest.configureTestCase(sinon);

require('sinon-as-promised')(Promise);
require('sinon-mongoose');

const Model = require('../../app/model');
const LoginError = require('../../app/model/errors').LoginError;
const RegisterError= require('../../app/model/errors').RegisterError;
const ValidationError = require('../../app/model/errors').ValidationError;

var model;
const testConfig = Object.freeze({
    db: {
        uri: 'mongodb://localhost/test'
    }
});

before(function(done) {
    logger.remove(logger.transports.Console); // Suppress logging
    mockgoose(mongoose).then(() => {
        model = Model(testConfig, mongoose, () => {
            done();
        });
    });
});

describe('Abstract User Model', function() {

    describe('#checkValid()', function () {
        it('should reject an empty name', function () {
            var testUser = new model.AbstractUser({
                lastName: 'LASTNAME',
                email: 'EMAIL@EMAIL.COM',
                username: 'USERNAME',
                password: 'PASSWORD'
            });
            var promise = testUser.checkValid();
            return assert.isRejected(promise, ValidationError);
        });

        it('should reject an empty last name', function () {
            var testUser = new model.AbstractUser({
                name: 'NAME',
                email: 'EMAIL@EMAIL.COM',
                username: 'USERNAME',
                password: 'PASSWORD'
            });
            var promise = testUser.checkValid();
            return assert.isRejected(promise, ValidationError);
        });

        it('should reject an empty email', function () {
            var testUser = new model.AbstractUser({
                name: 'NAME',
                lastName: 'LASTNAME',
                username: 'USERNAME',
                password: 'PASSWORD'
            });
            var promise = testUser.checkValid();
            return assert.isRejected(promise, ValidationError);
        });

        it('should reject an invalid email', function () {
            var testUser = new model.AbstractUser({
                name: 'NAME',
                lastName: 'LASTNAME',
                email: 'INVALIDEMAIL',
                username: 'USERNAME',
                password: 'PASSWORD'
            });
            var promise = testUser.checkValid();
            return assert.isRejected(promise, ValidationError);
        });

        it('should reject an empty username', function() {
            var testUser = new model.AbstractUser({
                name: 'NAME',
                lastName: 'LASTNAME',
                email: 'EMAIL@EMAIL.COM',
                password: 'PASSWORD'
            });
            var promise = testUser.checkValid();
            return assert.isRejected(promise, ValidationError);
        });

        it('should reject an empty password', function() {
            var testUser = new model.AbstractUser({
                name: 'NAME',
                lastName: 'LASTNAME',
                email: 'EMAIL@EMAIL.COM',
                username: 'USERNAME'
            });
            var promise = testUser.checkValid();
            return assert.isRejected(promise, ValidationError);
        });

        it('should accept a correct user', function() {
            var testUser = new model.AbstractUser({
                name: 'NAME',
                lastName: 'LASTNAME',
                email: 'EMAIL@EMAIL.COM',
                username: 'USERNAME',
                password: 'PASSWORD'
            });
            var promise = testUser.checkValid();
            return assert.isFulfilled(promise);
        });
    });

    describe('#hashPassword(), #checkPassword()', function() {
        it('should modify a User instance password', function() {
            const testPassword = 'testing';
            var testUser = new model.AbstractUser({
                username: 'test',
                password: testPassword
            });
            testUser.hashPassword().then(() => {
                assert(testUser.password !== testPassword);
            });
        });

        it('should leave a valid hash', function() {
            const testPassword = 'testing';
            var testUser = new model.AbstractUser({
                username: 'test',
                password: testPassword
            });
            testUser.hashPassword().then(assert(testUser.checkPassword(testPassword)));
        });
    });

    describe('#login()', function() {
        it('should resolve to an instance of the logged in user', sinon.test(function() {
            // Mock a User
            this.mock(model.AbstractUser)
                .expects('findOne').withArgs({ username: 'USERNAME' })
                .chain('lean')
                .chain('exec')
                .yields(null, new model.AbstractUser({
                    username: 'USERNAME',
                    password: '$2a$10$plrHzNtn2mHX8sP0rzxN0e0kFU9ih2f/pXuk.Hu0looZh3YDn/EQm'
                }));

            var promise = model.AbstractUser.login({
                username: 'USERNAME',
                password: 'PASSWORD'
            });
            return assert.eventually.deepPropertyVal(promise, 'constructor.modelName', 'AbstractUser');         
        }));

        it('should throw a LoginError if the user/password is incorrect', sinon.test(function() {
            // Mock an empty response
            this.mock(model.AbstractUser)
                .expects('findOne')
                .chain('lean')
                .chain('exec')
                .yields(null, null);

            var promise = model.AbstractUser.login({
                username: 'ANOTHER',
                password: 'PASSWORD'
            });
            return assert.isRejected(promise, LoginError);
        }));
    });

    describe('#signUp()', function(){
        it('should save the new user to the database', sinon.test(function(){            
            var newUser = {
                name: 'NAME',
                lastName: 'LASTNAME',
                email: 'EMAIL@EMAIL.COM',
                username: 'USERNAME',
                password: 'PASSWORD'
            };
            var saveStub = this.stub(model.AbstractUser(newUser), 'save');
            
            model.AbstractUser.signUp(newUser);
            return assert(saveStub);
        }));
    });
});

describe('Player User Model', function() {

    describe('#signUp()', function(){
        it('should save the new user to the database', sinon.test(function(){            
            var newUser = {
                name: 'NAME',
                lastName: 'LASTNAME',
                email: 'EMAIL@EMAIL.COM',
                username: 'USERNAME',
                password: 'PASSWORD'
            };
            var saveStub = this.stub(model.PlayerUser(newUser), 'save');
            
            model.PlayerUser.signUp(newUser);
            return assert(saveStub);
        }));
    });

});