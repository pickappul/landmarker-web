var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('mapa'), {
        center: {
            lat: -12.08196012,
            lng: -76.9000
        },
        zoom: 12
    });
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, hidePosition);
    } else {
        alert("La geolocalización no está soportada por su navegador. Estamos tratando de obtener su ubicación a través de su IP.");
        ipPosition();
    }
}

function showPosition(position) {
    pos = {
        lat: parseFloat(position.coords.latitude),
        lng: parseFloat(position.coords.longitude)
    };
}

function hidePosition(position) {
    alert('El usuario denegó el acceso a su ubicación. Estamos tratando de obtener su ubicación a través de su IP.');
    ipPosition();
}

function ipPosition() {
    $.get("https://ipinfo.io", function(response) {
        var loc = response.loc.split(',');
        pos = {
            lat: parseFloat(loc[0]),
            lng: parseFloat(loc[1])
        };
    }, "jsonp");
}

getLocation();
var markers = [];
var getMarkerUniqueId = function(lat, lng) {
    return lat + '_' + lng;
}

function addMarker(location) { // añade un marcador al mapa y lo agrega al arreglo
    var markerId = getMarkerUniqueId(location.lat, location.lng); //  se utiliza para almacenar en caché este marcador en los marcadores de objeto
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        animation: google.maps.Animation.DROP,
        id: markerId
    });
    markers[markerId] = marker;
}

function getCheckpoints() {
  var serviceRoute = "https://landmarker.meido.ninja/travel/near/10000/-12.08196012/-77.03596115"
  $.getJSON(serviceRoute, function(json) {
      for (i = 0; i < json.length; i++) {
        addCheckPointsMarkers(json[i].coordinates[0],json[i].coordinates[1],json[i].name);
      }
  });
}


var flag;
function addCheckPointsMarkers(Long,Lati,Name) {
    var myLatLng = {lat:Lati, lng:Long};
    var checkPointIcon = {
        url: 'https://erio.meido.ninja/u/1479623558426?key=JLywSKtNfE-tJOI3f5zDLllF'
      };
    flag = new google.maps.Marker({
        position: myLatLng,
        map: map,
        label:'',
        icon:checkPointIcon,
        animation: google.maps.Animation.DROP
    });
    var contentString = '<div style="">'+
      '<div style="">'+
      '</div>'+
      '<h1 id="firstHeading" class="firstHeading">'+Name+'</h1>'+
      '<div id="bodyContent">'+
      '<h3># checkpoints:'+' 5'+'</h3>'+ //(deberia venir del servicio)
      '<p> <b>Lorem</b>, also referred to as <b>Lorem 2</b>, '+
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '+
      'Aenean nibh magna, semper nec gravida vel, tempor non nisl. '+
      'Donec vel arcu sit amet arcu condimentum lobortis a a libero. '+
      'Etiam orci ex, sagittis pharetra ante sed, vestibulum laoreet leo. '+
      'Fusce urna purus, venenatis nec metus sodales, porta tincidunt mi. </p>'+
      '<p>Attribution: Lorem'+
      '(last visited June 22, 2070).</p>'+
      '</div>'+
      '</div>';
      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      flag.addListener('click', function() {
        infowindow.open(map, flag);
      });
}


$(document).ready(function() {
    var socket = io();
    var random = Math.floor((Math.random() * 100) + 1);

    $('#chat-container').submit(function() {
        var messg2 = 'LANDER ' + random + ': ' + $('#m').val();
        socket.emit('chat message', messg2);
        $('#m').val('');
        return false;
    });

    socket.on('chat message', function(msg) {
        $('#messages').append($('<li>').text(msg));
    });

    check_pos = setInterval(function() { // crea un bucle y espera la respuesta
        if (typeof pos != 'undefined') { // mientras que la posición no está definida se reintenta cada medio segundo
            socket.emit('new_user', {
                pos: pos
            });
            clearInterval(check_pos);
        }
    }, 500);
    socket.on('already', function(data) {
        $.each(data.visitors, function(key, pos) {
            addMarker(pos);
        });
    });
    socket.on('connected', function(data) {
        $("#users_count").html("<strong>" + data.users_count + "</strong>" + " usuarios conectados");
        $("#users_count").css({
            'visibility': 'visible'
        });
        addMarker(data.pos);
        getCheckpoints();
    });
    socket.on('disconnected', function(data) {
        // ahora se puede borrar la posición:
        var markerId = getMarkerUniqueId(data.del.lat, data.del.lng); // obtiene el id del marcador usando las coordenadas del punto
        var marker = markers[markerId]; // encuentra el marcador
        removeMarker(marker, markerId); // borra el marcador
        $("#users_count").html("<strong>" + data.users_count + "</strong>" + " usuarios conectados");
    });
    var removeMarker = function(marker, markerId) {
        marker.setMap(null); // establece en null el marcador encontrado para sacarlo del mapa
        delete markers[markerId]; // elimina la instancia marcador apartir del marcador de objeto
    };

});
