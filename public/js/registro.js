"use strict";

$(function(){

/**
 * Gets all the inputs with jQuery
 */
var name = $('#name');
var lastName = $('#lastName');
var username = $('#username');
var email = $('#email');
var birthDay = $('#birthDay');
var birthMonth = $('#birthMonth');
var birthYear = $('#birthYear');
var sex = $('#sex');
var password = $('#password');
var confirmPassword = $('#confirmPassword');

var birthParentDiv = $('#birthDate').parent();

/**
 * Function for adding an error message when the user clicks away 
 * from the input. 
 */
function addValidation (input, test, parent) 
{
	input.focusout(function(){
		showErrorIfInvalid(input, test, parent);
	});
}

/**
 * Function for showing an error message given a
 * input, a test for the input, and a parent that 
 * contains the input.
 */
function showErrorIfInvalid (input, test, parent) 
{
	if (typeof parent === 'undefined')
		parent = input.parent();

	if (test(input.val())){
		parent.children().last().hide();	
		return true;		
	}
	else{
		parent.children().last().show();
		return false;
	}
}

function showErrorMessage (input, msg, parent)
{
	if (typeof parent === 'undefined')
		parent = input.parent();

	if (msg !== '' || msg === undefined) 
		parent.children().last().text(msg);

	parent.children().last().show();
}


/**
 * The following functions are used to test the value
 * of the inputs.
 */
function validateEmail(email){  
	return (/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email));
}

function validateText(string){
	return ($.trim(string).length > 0);
}

function validateSelect(string){
	return (string !== 'empty');
}

function validatePassword(string){
	return (password.val() === string);
}

function validatebirthDay(dia){
	return (dia % 1 === 0 && dia > 0 && dia < 32);
}

function validatebirthYear(anio){
	return (anio % 1 === 0 && anio >= 1910 && anio <= moment().year());
}

/*
 * Function for showing a error if the date of birth
 * is not a valid date.
 */
function showErrorForCompleteDate(birthDate, parent){
	var oldestDate = {
		y: 1910,
		M: 0,
		d: 1
	};

	var isBirthValid = birthDate.isValid() &&
		birthDate.isAfter(moment(oldestDate)) && 
		birthDate.isBefore(moment());

	if (isBirthValid){
		parent.children().last().hide();
	}
	else{
		parent.children().last().show();

	}
	return isBirthValid;
}

function showResponseErrors(errors){

	if (errors.indexOf('name') >= 0) 
		showErrorMessage(name);

	if (errors.indexOf('lastName') >= 0) 
		showErrorMessage(lastName);

	if (errors.indexOf('username') >= 0) 
		showErrorMessage(username, 'Este nombre usuario ya existe.');

	if (errors.indexOf('email') >= 0) 
		showErrorMessage(email);

	if (errors.indexOf('birthDate') >= 0) 
		showErrorMessage(birthDay, '', birthParentDiv);

	if (errors.indexOf('sex') >= 0) 
		showErrorMessage(sex);

	if (errors.indexOf('password') >= 0) 
		showErrorMessage(password);	
}

/**
 * The following lines assign the focusout listener
 * to the given inputs.
 */
addValidation(name, validateText);
addValidation(lastName, validateText);
addValidation(username, validateText);
addValidation(email, validateEmail);
addValidation(birthDay, validatebirthDay, birthParentDiv);
addValidation(birthMonth, validateSelect, birthParentDiv);
addValidation(birthYear, validatebirthYear, birthParentDiv);
addValidation(sex, validateSelect);
addValidation(password, validateText);
addValidation(confirmPassword, validatePassword);

/**
 * Adds the action for the click event. First it checks
 * if all the inputs are valid. If all are valid it creates
 * a user and posts it to the server. Shows errors if any.
 */
$('#register').click(function(){
	var birthDate = moment({
		y: birthYear.val(),
		M: birthMonth.val(),
		d: birthDay.val()
	});

	var isUserValid = Boolean(showErrorIfInvalid(name, validateText) &
		showErrorIfInvalid(lastName, validateText) &
		showErrorIfInvalid(username, validateText) &
		showErrorIfInvalid(email, validateEmail) &
		showErrorIfInvalid(sex, validateSelect) &
		showErrorIfInvalid(confirmPassword, validatePassword) &
		showErrorForCompleteDate(birthDate, birthParentDiv) &
		showErrorIfInvalid(password, validateText));

	if (isUserValid) {
		var newUser = {
			name: name.val(),
			lastName: lastName.val(),
			username : username.val(),
			email: email.val(),
			birthDate: birthDate.format(),
			sex: sex.val(),
			password: password.val()
		};

		$.post('/register', newUser, function(data){
			if (data.errors !== undefined) {
				showResponseErrors(data.errors);
			}
			else{
				$('#register-modal').modal('hide');
			}
		});
	}
});

});