$(function(){
	var editPoint = $('.edit-point');
	var checkpoint = $('#checkpoints');

	function removeOption(){
		var pointNum = $(this).data('point');
		var optionNum = $(this).data('option');
		$('.option-groups-'.concat(pointNum, '-', optionNum)).remove();

		optionNum++;
		var group = $('.option-groups-'.concat(pointNum, '-', optionNum));
		while(group.length > 0){
			group.removeClass('option-groups-'.concat(pointNum, '-', optionNum));
			group.addClass('option-groups-'.concat(pointNum, '-', optionNum - 1));

			$(this).data('option', optionNum - 1);

			var input = group.find('input');
			var name = input.attr('name');
			var nameSplit = name.split('-');
			nameSplit[3] = optionNum - 1;
			input.attr('name', nameSplit.join('-'));

			group.find('label').text('Opción '.concat(optionNum - 1));

			optionNum++;
			group = $('.option-groups-'.concat(pointNum, '-', optionNum));
		}
		$('.but-add-opt-'.concat(pointNum)).data('next-option', optionNum - 1);
	}

	function addOption(e){
		var item = $('.option-group').clone();

		var point = $(this).data('point');
		var nextOption = $(this).data('next-option');
		
		item.removeClass('option-group');
		item.addClass('option-groups-'.concat(point, '-', nextOption));
		item.find('label').text('Opcion '.concat(nextOption));
		item.find('input').attr('name', 'checkpoint-option-'.concat(point, '-', nextOption));

		var button = item.find('button');
		button.data('point', point);
		button.data('option', nextOption);
		button.click(removeOption);

		$(this).data('next-option', ++nextOption);
		$(this).before(item);
	}

	function removePoint(){
		var pointNum = $(this).data('point');
		$('.edit-point-'.concat(pointNum)).remove();

		pointNum++;
		var nextPoint = $('.edit-point-'.concat(pointNum));
		while(nextPoint.length > 0){
			nextPoint.removeClass('edit-point-'.concat(pointNum));
			nextPoint.addClass('edit-point-'.concat(pointNum-1));

			$.each(nextPoint.find('input'), function(key, value){
				var val = $(value);
				var name = val.attr('name');

				var nameSplit = name.split('-');
				nameSplit[2] = pointNum-1;
				val.attr('name', nameSplit.join('-'));
			});

			nextPoint.find('button').data('point', pointNum - 1);

			var heading = nextPoint.find('.panel-title');
			var title = heading.text();
			var titleSplit = title.split(' ');
			titleSplit[1] = pointNum - 1;
			heading.text(titleSplit.join(' '));

			pointNum++;
			nextPoint = $('.edit-point-'.concat(pointNum));
		}
		$('#but-add-point').data('next-point', pointNum - 1);
	}

	function addCheckPoint(){
		var editPointClone = editPoint.clone();
		var pointNum = $(this).data('next-point');

		editPointClone.removeClass('edit-point');
		editPointClone.removeClass('hidden');
		var option = editPointClone.find('.option-group');
		option.removeClass('option-group');
		option.addClass('option-groups-'.concat(pointNum, '-', 1));
		editPointClone.addClass('edit-point-'.concat(pointNum));

		editPointClone.find('.panel-title').text('Checkpoint '.concat(pointNum));

		var val;
		$.each(editPointClone.find('input'), function(key, value){
			val = $(value);
			var name = val.attr('name');
			val.attr('name', name.concat('-', pointNum));
		});
		val.attr('name', val.attr('name').concat('-1'));

		editPointClone.find('button').data('point', pointNum);
		editPointClone.find('.but-add-option').click(addOption);
		editPointClone.find('.remove-point').click(removePoint);
		editPointClone.find('.remove-option').click(removeOption);

		$(this).data('next-point', ++pointNum);
		checkpoint.append(editPointClone);
	}

	function editClick(){
		$('input, textarea').prop("disabled", false);
		$('#but-add-point').removeClass('hidden');
		$('.but-add-option').removeClass('hidden');
		$('#update-travel').removeClass('hidden');
		$('.close').removeClass('hidden');
		$('#edit-travel').remove();
	}

	function fadeAlert(){
		setTimeout(function(){
			$.each($('.alert'), function(key, value){
				var elem = $(value);
				if (elem.hasClass('hidden') === false) {
					elem.fadeOut("slow", function(){
						elem.addClass('hidden');
					});
				}
			});
		}, 5000);
	}

	function createTravel(){
		var form = $('form');
        $.ajax({
			type: 'post',
			url: window.location.href,
			data: form.serialize(),
			success: function(msg){
				window.location.href = "/admin/travels";
			},
			error: function(err){
				console.log(err);
				$('#saving-travel-error').text(err.responseText);
				$('#saving-travel-success').addClass('hidden');
				$('#saving-travel-error').removeClass('hidden');
			}
        });
        fadeAlert();
	}

	function updateTravel(){
        var form = $('form');
        $.ajax({
			type: 'put',
			url: window.location.href,
			data: form.serialize(),
			success: function(msg){
				console.log(msg);
				$('#saving-travel-error').addClass('hidden');
				$('#saving-travel-success').removeClass('hidden');
			},
			error: function(err){
				console.log(err);
				$('#saving-travel-error').text(err.responseText);
				$('#saving-travel-success').addClass('hidden');
				$('#saving-travel-error').removeClass('hidden');
			}
        });
        fadeAlert();
	}

	function deleteTravel(){
		$.ajax({
			type: 'delete',
			url: window.location.href,
			success: function(data){
				if (data.msg === 'ok') window.location.href = "/admin/travels";
			},
			error: function(){
				$('#saving-travel-error').text('Error al eliminar');
				$('#saving-travel-success').addClass('hidden');
				$('#saving-travel-error').removeClass('hidden');
			}
		})
	}

	$('.remove-point').click(removePoint);
	$('.remove-option').click(removeOption);	
	$('.but-add-option').click(addOption);
	$('#but-add-point').click(addCheckPoint);
	$('#edit-travel').click(editClick);
	$('#delete-travel').click(deleteTravel);
	$('#update-travel').click(updateTravel);
	$('#create-travel').click(createTravel);
});