var gulp = require('gulp');
var less = require('gulp-less');
var browserSync = require('browser-sync').create();
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

// Copy CSS to dist
gulp.task('css', function() {
    return gulp.src(['public/css/*.css'])
        .pipe(gulp.dest('public/dist/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

//Minify compiled CSS
gulp.task('minify-css', ['css'], function() {
    return gulp.src('public/css/*.css')
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('public/dist/css'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

//Copy JS to dist
gulp.task('js', function() {
    return gulp.src(['public/js/*.js'])
        .pipe(gulp.dest('public/dist/js'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// Minify JS
gulp.task('minify-js', ['js'], function() {
    return gulp.src('public/js/*.js')
        .pipe(uglify())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('public/dist/js'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

// Copy vendor libraries from /bower_components into /vendor
gulp.task('copy', function() {
    gulp.src(['bower_components/bootstrap/dist/**/*', '!**/npm.js', '!**/bootstrap-theme.*', '!**/*.map'])
        .pipe(gulp.dest('public/vendor/bootstrap'));

    gulp.src(['bower_components/bootstrap-social/*.css', 'bower_components/bootstrap-social/*.less', 'bower_components/bootstrap-social/*.scss'])
        .pipe(gulp.dest('public/vendor/bootstrap-social'));

    gulp.src(['bower_components/datatables/media/**/*'])
        .pipe(gulp.dest('public/vendor/datatables'));

    gulp.src(['bower_components/datatables-plugins/integration/bootstrap/3/*'])
        .pipe(gulp.dest('public/vendor/datatables-plugins'));

    gulp.src(['bower_components/datatables-responsive/css/*', 'bower_components/datatables-responsive/js/*'])
        .pipe(gulp.dest('public/vendor/datatables-responsive'));

    gulp.src(['bower_components/flot/*.js'])
        .pipe(gulp.dest('public/vendor/flot'));

    gulp.src(['bower_components/flot.tooltip/js/*.js'])
        .pipe(gulp.dest('public/vendor/flot-tooltip'));

    gulp.src(['bower_components/font-awesome/**/*', '!bower_components/font-awesome/*.json', '!bower_components/font-awesome/.*'])
        .pipe(gulp.dest('public/vendor/font-awesome'));

    gulp.src(['bower_components/jquery/dist/jquery.js', 'bower_components/jquery/dist/jquery.min.js'])
        .pipe(gulp.dest('public/vendor/jquery'));

    gulp.src(['bower_components/metisMenu/dist/*'])
        .pipe(gulp.dest('public/vendor/metisMenu'));

    gulp.src(['bower_components/morrisjs/*.js', 'bower_components/morrisjs/*.css', '!bower_components/morrisjs/Gruntfile.js'])
        .pipe(gulp.dest('public/vendor/morrisjs'));

    gulp.src(['bower_components/raphael/raphael.js', 'bower_components/raphael/raphael.min.js'])
        .pipe(gulp.dest('public/vendor/raphael'));

});

// Run everything
gulp.task('default', ['minify-css', 'minify-js', 'copy']);

// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        proxy: 'localhost:3000'
    });
});

function reloadBrowser(){
    setTimeout(browserSync.reload, 3000);
}

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'minify-css', 'minify-js'], function() {
    gulp.watch('public/css/*.css', ['minify-css']);
    gulp.watch('public/js/*.js', ['minify-js']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch('views/*', reloadBrowser);
    gulp.watch('public/dist/js/*.js', reloadBrowser);
    gulp.watch('public/dist/css/*.css', reloadBrowser);
});
