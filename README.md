# Landmarker - Web App

----
## Sobre

Landmarker es un juego con el propósito de que los usuarios conozcan más de la ciudad a través de desafios.

---
## Requerimientos

 1. [Node.js](https://nodejs.org/en/)
 2. [MongoDB](https://www.mongodb.com/download-center#community)
 3. [Bower](https://bower.io/)
 4. [Gulp](http://gulpjs.com/)

----
## Uso

Luego de descargar el proyecto, se tiene que cambiar de nombre al archivo config.default.json a config.json. En este se aloja la configuración básica del servidor web. Se puede utilizar el siguiente comando.

    cp config.default.json config.json

Además, iniciar el servicio redis y la base de datos mongo.

Es necesario instalar todos los paquetes indicados en package.json para nodejs y en bower.json para bower.

    npm install
    bower install

Para correr el proyecto primero es necesario copiar todos los paquetes a la carpeta vendor necesarios para la interface web. Se puede utilizar:

    gulp

Los anteriores tres comandos tambien se pueden ejecutar utilizando el siguiente comando.

	npm run build

Luego ya se puede iniciar el proyecto con normalidad.

    node app.js

----
## Desarrollo

Nosotros hemos implementado algunos tasks en gulp para facilitar el desarrollo. Si se quiere activar watcher para los archivos que consideramos necesarios se puede ejecutuar el siguiente comando:

    gulp dev

Las pruebas unitarias se realizan utilizando Mocha.

    npm test
